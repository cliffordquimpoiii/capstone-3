import React, {useState,useEffect} from 'react';
import {Table} from 'react-bootstrap';
import AddItem from './AddItem';
import EditItem from './EditItem';
import ArchiveItem from './ArchiveItem';



export default function AdminView(props) {
	const {itemsData, fetchData} = props;
	const [items, setItems] = useState([]);

	useEffect(() =>{
		const itemsArr = itemsData.map(items =>{
			return(
				<tr key={items._id}>
					<td>{items._id}</td>
					<td>{items.name}</td>
					<td>{items.description}</td>
					<td>{items.price}</td>
					<td className={items.isActive ? "text-success" : "text-danger"}>
						{items.isActive? "Available" : "Unavailable"}
					</td>
					<td><EditItem items={items._id} fetchData={fetchData} /></td>
					<td><ArchiveItem items={items._id} isActive={items.isActive} fetchData={fetchData} /></td>
					
					
				</tr>

				)
		})
		setItems(itemsArr);
	}, [itemsData,fetchData])


	return (
		<>
			<div className ="text-center my-4">
				<h1>Admin Dashboard </h1>
			{	<AddItem fetchData={fetchData} />}
				
			</div>
			<Table striped bordered hover responsive>
				<thead className ="bg-dark text-white">
					<tr>
						<th className = "text-center">ID</th>
						<th className = "text-center">Name</th>
						<th className = "text-center">Description</th>
						<th className = "text-center">Price</th>
						<th className = "text-center">Availability</th>


						<th colspan="3" className = "text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					{items}
				</tbody>
			</Table>

		</>
		)
}