import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function SpecificItem() {

	const navigate = useNavigate();

	//useParams() contains any values we are tryilng to pass in the URL stored
	//useParams is how we receive the courseId passed via the URL
	const { itemId } = useParams();
	const [count, setCount] = useState(1);
	

	useEffect(() => {

		fetch(`https://fathomless-inlet-91333.herokuapp.com/products/${itemId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	} ,[])

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
    const [total, setTotal] = useState('')
    const [decBut, setDecBut] = useState(false);
    const [incBut, setIncBut] = useState(true);
	
	
	// const incrementCount = () =>{
	//     setCount(count +1);
	// 	setTotal(count * price);
	
	

	// }

	// const decrementCount = () =>{
	// 	if(count !=0){
		
	// 	 setCount(count - 1);
	// 	 setTotal(total - price);
	// 	 setTotal(count * price);
		 
	//   }
		
 //    }

	// const totalP = () =>{
	// 	total = price*count;
	// 	setTotal(total);
	// }




	//enroll function
	const order = (itemsId) => {
		fetch(`https://fathomless-inlet-91333.herokuapp.com/order/order`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				itemId: itemsId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Successfully ordered',
					icon: 'success'
				})

				navigate('/items')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}


	return (
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
					<Card.Text>Quantity: {count} </Card.Text>
					<Button variant="secondary" onClick={() =>{setCount(count -1);
						setTotal(total-price);
					
					}}
					disabled={decBut}>-</Button>


					<Button variant="secondary" onClick={() =>{setCount(count +1);
						setTotal(total+price);
					
					}}
					disabled={incBut}>+</Button>
					<Card.Text>Total: {total} </Card.Text>
				</Card.Body>

				<Card.Footer>

					{user.accessToken !== null ?
					
						<div className="d-grip gap-2">
							<Button variant="primary" onClick={() => order(itemId)}>Add to Cart</Button>
						</div>
						
						
						
						
					
					

						:

						<Link className="btn btn-warning d-grip gap-2" to="/login">Login to Order</Link>
					}
					 
				</Card.Footer>
			</Card>
		</Container>

		)
}
