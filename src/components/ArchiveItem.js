import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveItem({items, isActive, fetchData}) {

	const archiveToggle = (itemId) => {
		fetch(`https://fathomless-inlet-91333.herokuapp.com/products/${itemId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Item successfully disabled'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}

	const unarchiveToggle = (itemId) => {
		fetch(`https://fathomless-inlet-91333.herokuapp.com/products/${itemId}/reactivate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Item successfully enabled'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}
 
 

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(items)}>Deactivate</Button>

				:

				<Button variant="success" size="sm" onClick={() => unarchiveToggle(items)}>Activate</Button>

			}
		</>

		)
}