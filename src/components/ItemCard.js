import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
export default function ItemCard ({itemProp}){

	const  {_id, name, description, price} = itemProp;

	
	return(
		<Row className ="rowCard">
			<Col>
				<Card className = "itemCard p-3">
					<Card.Body>
						<Card.Title>
							<h2>{name}</h2>
						</Card.Title>
						<Card.Text className = "my-3">
							<h4>Description:</h4>
								{description}
						</Card.Text>
						<Card.Text>
							<h4>Price:</h4>
							Php {price}
						</Card.Text>
						<Link className="btn btn-primary" to={`/products/${_id}`}> View Item
						</Link>

					</Card.Body>
						
				</Card>

			</Col>
				
			
		</Row>

		)
}


ItemCard.propTypes = {
		itemProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired

	})
}