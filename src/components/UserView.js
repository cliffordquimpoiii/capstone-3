import React, {useState, useEffect} from 'react';
import ItemCard from './ItemCard';

export default function UserView({itemsData}) {
	const [items, setItems] = useState([])
	useEffect(()=>{
		const itemsArr = itemsData.map(items =>{
			if(items.isActive === true){
				return (
					<ItemCard itemProp={items} key={items._id} />
					)
			}
			else{
				return null;
			}
		})
				setItems(itemsArr);
	}, [itemsData])	

	return (
		<>
			{items}
		</>
		)
}