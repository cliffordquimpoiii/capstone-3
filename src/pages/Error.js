import React from 'react';
import {Link} from 'react-router-dom';


export default function Error(){
	return (
	<>
		<h1>404 PAGE NOT FOUND</h1>
		<h5>Go back to the <Link to="/"> homepage</Link></h5>
		
	</>
	)
}