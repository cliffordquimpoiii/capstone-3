import React,{useState, useContext, useEffect} from 'react';
import UserView from '../components/UserView';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';


export default function Items() {
	const {user} = useContext(UserContext);
	const [allItems, setAllItems] = useState([]);
	
	const fetchData = () => {
		fetch('https://fathomless-inlet-91333.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllItems(data)

		})
	}

	useEffect(()=>{
		fetchData()
	}, [])
	return (
	<>
		{(user.isAdmin === true)?
		<AdminView itemsData={allItems} fetchData={fetchData} />
		:
		<UserView itemsData={allItems}/>
		 }
	</>

	
		)
}